package com.example.fragmentstackexample;

import java.util.Stack;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fragmentstackexample.fragments.FragmeneOne;
import com.example.fragmentstackexample.fragments.FragmentThree;
import com.example.fragmentstackexample.fragments.FragmentTwo;

public class FragmentStackMainActivity extends ActionBarActivity {
	private FragmentManager fManager;
	private FragmentTransaction fTransaction;
	private Fragment currentFragment;
	private Stack<Integer> fragmentStack;
	private ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fragment_stack_main);

		fManager = getSupportFragmentManager();
		fragmentStack = new Stack<Integer>();
		if (savedInstanceState == null) {
			fTransaction = fManager.beginTransaction();
			fTransaction.replace(R.id.container, new PlaceholderFragment());
			// fragmentStack.push(android.R.id.home);
			// fTransaction.addToBackStack(null);
			fTransaction.commit();
		}
		fManager.addOnBackStackChangedListener(new OnBackStackChangedListener() {

			@Override
			public void onBackStackChanged() {
				Log.d("Count entry", "" + fManager.getBackStackEntryCount());

				if (fManager.getBackStackEntryCount() == 0) {
					actionBar.setDisplayHomeAsUpEnabled(false);
				} else {
					actionBar.setDisplayHomeAsUpEnabled(true);
				}

				// if (fragmentStack.isEmpty()) {
				// actionBar.setDisplayHomeAsUpEnabled(true);
				// } else {
				// actionBar.setDisplayHomeAsUpEnabled(false);
				// }

			}
		});
		actionBar = getSupportActionBar();
		actionBar.setHomeButtonEnabled(true);
		// actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.fragment_stack_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		boolean isHomePressed = false;
		int id = item.getItemId();
		currentFragment = null;
		switch (id) {
		case android.R.id.home:
			isHomePressed = true;
			currentFragment = new PlaceholderFragment();
			for (int i = 0; i < fManager.getBackStackEntryCount(); i++) {
				fManager.popBackStack();
			}
			fragmentStack.clear();
			Log.d("Fragment stack value ::", "" + fragmentStack.size());
			break;
		case R.id.action_settings:
			return true;
		case R.id.action_fragment1:
			currentFragment = new FragmeneOne();
			break;
		case R.id.action_fragment2:
			currentFragment = new FragmentTwo();
			break;
		case R.id.action_fragment3:
			currentFragment = new FragmentThree();
			break;
		default:
			Log.d("FragmentStackActivity", "This is default selection");
			break;
		}
		fTransaction = fManager.beginTransaction();
		fTransaction.replace(R.id.container, currentFragment);
		if (!isHomePressed) {
			fTransaction.addToBackStack(null);
		}
		fTransaction.commit();
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_initial,
					container, false);
			((TextView) rootView.findViewById(R.id.fragment_number))
					.setText("Initial Fragment");
			return rootView;
		}
	}

	@Override
	public void onBackPressed() {
		if (!fragmentStack.isEmpty()) {
			fragmentStack.pop();
		} else {
			super.onBackPressed();
		}
	}

}
