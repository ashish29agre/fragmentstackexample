package com.example.fragmentstackexample.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fragmentstackexample.R;

public class FragmeneOne extends Fragment implements OnClickListener{
	private View fragmentView;
	private Button triggerButton;
	private ImageView animatedView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		fragmentView = inflater.inflate(R.layout.fragment_one, container, false);
		((TextView)fragmentView.findViewById(R.id.fragment_number)).setText("1");
		triggerButton = (Button) fragmentView.findViewById(R.id.trigger_btn);
		triggerButton.setOnClickListener(this);
		animatedView = (ImageView) fragmentView.findViewById(R.id.animated_view);
		return fragmentView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		if(animatedView.getVisibility() == View.VISIBLE) {
			Animation out = AnimationUtils.makeOutAnimation(getActivity(), true);
			animatedView.startAnimation(out);
			animatedView.setVisibility(View.INVISIBLE);
		} else {
			Animation in = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
			animatedView.startAnimation(in);
			animatedView.setVisibility(View.VISIBLE);
		}
	}
}