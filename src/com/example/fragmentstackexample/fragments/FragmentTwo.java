package com.example.fragmentstackexample.fragments;

import com.example.fragmentstackexample.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentTwo extends Fragment{
	private View fragmentView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		fragmentView = inflater.inflate(R.layout.fragment_two, container, false);
		((TextView)fragmentView.findViewById(R.id.fragment_number)).setText("2");
		return fragmentView;
	}
}
